<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'kharenko.me');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', '192.168.0.101');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g:OPK=JM**_Y,0.*3H6-%y2a_SQ@!CF>x(+hkA;`Z+D}qJ>xQfmS|>5hnfsafrnf');
define('SECURE_AUTH_KEY',  'c0QhNlX`o3Y^!LN1_wNznWTDv ~C[}($ALSs|(b4Q|_S4(@+o6#K*[p!%.tf<&6s');
define('LOGGED_IN_KEY',    ')~;rGlc.:p2u)!-;Eph/5yl  <)_(YuTQt~.#b5A$4PezR$P@>vu$n|eb/*4*5}v');
define('NONCE_KEY',        'O!IACE;J%A~xdfw4Q38-8>H(1nqUI=6%5wj1@h8|]nfBQiAtT{Dj<7p~2w&BQTN9');
define('AUTH_SALT',        '{()N3EO9eJ!&_Am+2p~*>{+k$o0FHHj*b-kqo`lr;pD+R<[YUZ|L]N|3UCRlaOM{');
define('SECURE_AUTH_SALT', '(8E&iv`NpYRK<&Pvw@--Qi{oE|Wof}EiI(w$%?-jO;-kffR`s~ytOGVN1!0r=)8S');
define('LOGGED_IN_SALT',   '%-|4emf^D]PRG)6wN@#|HzTi?d[KEg]$<4X3TM6p|)R )hvV{tV/F!WM:z4=K|+n');
define('NONCE_SALT',       'D(k-R:}X|U-l7u|=m[:[ 2+{ImweY3d-4/`uDprw@m5[>RO3&lkjH9{:`lCwZ$<N');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'base_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');

