<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();?>

    <div class="slider">
        <div slider="allPhotos" style="display: none;">
            <? $images = get_post_gallery_images( $post );

            for($i = 0; $i < count($images); $i++){

                $image = preg_replace('/(\-150x150)/isu', '', $images[$i]);
                $mainPage = '';
                if($i === 0) {
                    $mainPage = 'mainPhoto="true"';
                }
                echo '<img '.$mainPage.' src="'.$image.'">';
            }

            ?>
        </div>
        <!--TODO: set navigation aside of slider-->
        <div class="slide" slider="slide">
            <table class="navigation">
                <tr>
                    <td class="prevBtn" slider="prevBtn">
                        <img src="<?=get_stylesheet_directory_uri();?>/images/arrow_left.png"/></td>
                    <td class="nextBtn" slider="nextBtn">
                        <img src="<?=get_stylesheet_directory_uri();?>/images/arrow_right.png"/></td>
                </tr>
            </table>
        </div>
    </div>

<? get_footer();

