<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();?>

    <div slider="allPhotos" style="display: none;">
        <? $images = get_post_gallery_images( $post );

        for($i = 0; $i < count($images); $i++){

            $image = preg_replace('/(\-150x150)/isu', '', $images[$i]);

            $mainPage = '';
            if($i === 0) {
                $mainPage = 'mainPhoto="true"';
            }
            echo '<img '.$mainPage.' src="'.$image.'">';
        }

        ?>
    </div>

    <div class="galleryWrapper">
        <?
        echo '<div class="column">';

        $imagesInColumn = ceil(count($images)/4);
        for($i = 0; $i < count($images); $i++){

            $image = preg_replace('/(\-150x150)/isu', '', $images[$i]);

            echo '<img src="'.$image.'">';
            if(($i+1)%$imagesInColumn === 0){
                echo '</div>';
                echo '<div class="column">';
            }
        }
        echo '</div>';

        ?>
        <div style="clear: both"></div>
    </div>
    <div class="slider" slider="slider" style="display: none;">
        <div class="close" style="display: none;"></div>
        <div class="slide" slider="slide">
            <table class="navigation">
                <tr>
                    <td class="prevBtn" slider="prevBtn">
                        <img src="<?=get_stylesheet_directory_uri();?>/images/arrow_left.png"/></td>
                    <td class="nextBtn" slider="nextBtn">
                        <img src="<?=get_stylesheet_directory_uri();?>/images/arrow_right.png"/></td>
                </tr>
            </table>
        </div>
    </div>

<? get_footer();
