<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

$page = preg_replace( '/\//', '',$_SERVER['REQUEST_URI']);

if ($page === '') $page = 'home';

$title = array(
    'home' => 'Oksana Kharenko Photographer',
    'gallery' => 'Gallery',
);

?>
<!DOCTYPE html>
<html>
<head>
    <title><?=$title[$page]?></title>
    <link rel="icon" href="<?=get_stylesheet_directory_uri();?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=get_stylesheet_directory_uri();?>/favicon.ico" type="image/icon">
    <link rel="shortcut icon" href="<?=get_stylesheet_directory_uri();?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?=get_stylesheet_directory_uri();?>/favicon.ico" type="image/icon">

    <link rel="stylesheet" href="<?=get_stylesheet_directory_uri();?>/css/style.css"/>
    <link rel="stylesheet" href="<?=get_stylesheet_directory_uri();?>/css/<?=$page?>.css"/>
    <?require('page-templates/'.$page.'/head.php');?>
</head>
<body>
<header>
    <a href="/" class="logo"></a>
</header>
<div class="content">
    <nav class="menu">
        <b><a class="menuItem
                    <?if ($page === 'home'): ?> active <?endif?>"
              href="/">HOME</a></b><br/>

        <b><a class="menuItem
                    <?if ($page === 'gallery'): ?> active <?endif?>"
              href="/gallery">GALLERY</a></b><br/>

        <b><a class="menuItem
                    <?if ($page === 'shop'): ?> active <?endif?>"
              href="#">SHOP</a></b><br/>
    </nav>
    <article>
        <?require('page-templates/'.$page.'/content.php');?>
