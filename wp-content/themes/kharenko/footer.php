<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
        </article>
        </div>
        <footer>
            <a href="#" class="copyright">© PineApple Team LTD 2015</a>
            <div class="socialButtons">
                <a href="https://vk.com/oxanakharenko" target="_blank" class="vk"></a>
<!--                <a href="#" class="twitter"></a>-->
                <a href="#" target="" class="instagram"></a>
            </div>
        </footer>
    </body>
</html>
<!--TODO: добавить полупрозрачные белые кругляши к стрелкам-->