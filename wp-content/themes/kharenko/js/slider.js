/**
 *
 * Requirements:
 *     Slide element must be a 'div' element.
 *
 * Input argument is object, that may contain:
 *     thisPluginAttrName('slider' by default);
 *     fadeTime(100 by default);
 *
 * Before use you must make (invisible) block of images
 * you want to be shown in slider. To this block you
 * must apply argument slider="allPhotos".
 *
 * Then set new attributes to following elements:
 *     "show next photo" button: slider="prevBtn";
 *     "show previous photo" button: slider="nextBtn";
 *     output images block: slider="slide";
 *
 *
 * If you need you can use any name instead of 'slider',
 * but for that you must set it in 'thisPluginAttrName'.
 *
 */
$.Slider = function (opts) {
    var self = this;
    
    var opts = opts || {};
    var attrName = opts.attrName || 'slider';
    var fadeTime = opts.fadeTime || 100;
    var prevBtnSelector = '['+attrName+'="prevBtn"]';
    var nextBtnSelector = '['+attrName+'="nextBtn"]';
    var allImagesSelector = '['+attrName+'="allPhotos"]';
    var slideSelector = '['+attrName+'="slide"]';

    var currentPhotoNum = 0;
    var allImages = $(allImagesSelector + " > img");

    var slide = $(slideSelector);
    
    var allImagesSrcs = $.map(allImages, function (elem, i) {
        if ($(elem).attr('mainPhoto')) {
            currentPhotoNum = i;
        }
        return elem.src;
    });

    slide.css(
        'background-image',
        'url(' + allImagesSrcs[currentPhotoNum] + ')'
    );

    var incrementInRange = function () {
        if (currentPhotoNum < allImagesSrcs.length - 1) {
            currentPhotoNum++;
        } else {
            currentPhotoNum = 0;
        }
    };

    var decrementInRange = function () {
        if (currentPhotoNum > 0) {
            currentPhotoNum--;
        } else {
            currentPhotoNum = allImagesSrcs.length - 1;
        }
    };

    var naviBtnOnClick = function (action) {
        return function () {
            slide.fadeOut(fadeTime, function () {
                action();
                slide.css('background-image', 'url(' + allImagesSrcs[currentPhotoNum] + ')');
                slide.fadeIn(fadeTime);
            });
        }
    }

    $(prevBtnSelector).click(
        naviBtnOnClick(incrementInRange)
    );

    $(nextBtnSelector).click(
        naviBtnOnClick(decrementInRange)
    );

    /**
     * Prevents ".slide" to be selected as text.
     */
    $(".slide").on('mousedown', function () {
        return false;
    });

    self.setPhotoByURL = function (url) {
        currentPhotoNum = allImagesSrcs.indexOf(url);
        slide.css('background-image', 'url(' + allImagesSrcs[currentPhotoNum] + ')');
    };
};

